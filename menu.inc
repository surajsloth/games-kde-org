<?php
$this->setName ("KDE Games Center");

$section =& $this->appendSection("Information");
$section->appendLink("Front Page","", true, false, $this->menu_baseurl.'pics/icons/aboutus.png');
$section->appendLink("KDE","http://www.kde.org/",false, false, $this->menu_baseurl.'pics/icons/about-kde.png');
$section->appendLink("News","news.php", true, false, $this->menu_baseurl.'pics/icons/news.png');
$section->appendLink("Team","team.php", true, false, $this->menu_baseurl.'pics/icons/team.png');
$section->appendLink("Get KDE Games","get.php", true, false, $this->menu_baseurl.'pics/icons/get.png');

$section =& $this->appendSection("Games Selection");
$dir = scandir('categories');
if (is_array($dir)) {
	foreach($dir as $cat) {
		if (is_file('categories/'.$cat.'/info.xml')) {
			$category = new simpleXmlElement(file_get_contents('categories/'.$cat.'/info.xml'));
			$category = $category->category;
			$section->appendLink(trim($category->name),"category.php?category=".$cat, true, false, $this->menu_baseurl.'categories/'.$cat.'/icon.png');
		}
	}
}
$section->appendLink("Older Games","./old",false, false, $this->menu_baseurl.'pics/icons/old.png');
?>
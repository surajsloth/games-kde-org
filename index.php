<?php
	$page_title = "The KDE Games Center";
	$rss_feed_link = "http://games.kde.org/rss.php";
	$rss_feed_title = "KDEGames News and Updates";
	
	include "header.inc";
	include('gamesHeader.php');
?>
<div class="Home_header">
	<div class="Home_back">
		<div>
			<table>
			<tr>
			<td style="vertical-align: top;color:#00224d;">
				<b>KDEGames</b> is a division of the <br /><b>KDE community</b>.
				<p>The main focus of our team is to provide desktop users with high quality gaming and entertainment software.</p>
				<p>We realize that games are first and foremost about fun and our greatest ambition is to create only the most enjoyable and amusing playware.<br />The <b>KDEGames</b> team achieves this goal by paying greater attention to gaming details, using the latest <b>KDE</b> based technology, and listening closely to our users' feedback.</p>
				<p>To date <b>KDEGames</b> has accumulated an impressive selection of quality games in nearly all genres. Each of these small masterpieces have been carefully tailored to satisfy the demands of all, even the most pretentious gamers.</p>
			</td>
			<td  style="vertical-align: top;">
				<img src="pics/kdegames.png" alt="" width="248" height="234" />
			</td>
			</tr>
			</table>
		</div>
	</div>
	<br />
</div>
<h1>Random Game</h1>
<?php
	$games = scandir( 'games' );
	$game = rand( 3, count($games) -1 );
	$gameName = $games[$game];
	$xml = simplexml_load_file( './games/'.$gameName.'/info.xml' );
	$game = $xml->game;
	include('GorgImage.php');
	$Blocks[] = Array( 'title'=>'Random Game', 'content' => '<a href=game.php?game='.$gameName.'>'.$game->name.'<br /><img src="./games/'.$gameName.'/thumbnail.jpg" alt="'.$game->name.'"/></a>' );
	new GorgImage($game->name, $game->description.'Click the image for more information, a Howto and Screenshots.', './games/'.$gameName.'/thumbnail.jpg', 'game.php?game='.$gameName, './games/'.$gameName.'/icon.png');

	new BlueBox("<div style='float:left;margin-right:10px;'><img src='pics/info.png' alt='' /></div><div style='text-align:justify;'>For your convenience the games have been split into categories by genre and then sorted alphabetically. While browsing through this website please use the selection menu bar located on the left hand side to navigate through the different categories.</div>");
?>
<?php
new BlueBox("<div style='float:left;margin-right:10px;'><img src='pics/info.png' alt='' /></div><div style='text-align:justify;'>Additionally, we want you to remember that a large part of our success comes from careful and considerate review of users' feedback and comments. Meaning that YOU can help us to make these games even more fun to play. Send us your thoughts and suggestions today and enjoy them being taken into account in our next big release.</div>");
?>
<img src="./pics/icons/rss.png" alt="RSS Feed" />
<a href="./news.rdf">
	 Subscribe To Our News Feeds
</a>
<?php
  include "footer.inc";
?>

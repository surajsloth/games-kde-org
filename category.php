<?php

	$category = $_GET['category'];
	if (!$category) {
		include 'header.inc';
		print 'Please Select a Category';
		include 'footer.inc';
	}

	if (!file_exists('categories/'.$category.'/info.xml')) {
		include 'header.inc';
		print 'Cannot Find the Specified Category';
		include 'footer.inc';
	}

	$category = new simpleXmlElement(file_get_contents('categories/'.$category.'/info.xml'));
	$category = $category->category;
	$page_title = $category->name." Games";
	include "header.inc";
	include('gamesHeader.php');

	if ( count($category->games->game) > 0) {
		include "GorgImage.php";
		foreach($category->games->game as $game) {
			$gameName= trim($game);

			if (file_exists('./games/'.$gameName.'/info.xml')) {
				$game = new simpleXmlElement(file_get_contents('./games/'.$gameName.'/info.xml'));
				$game = $game->game;
				$icon = './games/'.$gameName.'/icon.png';
				if (!file_exists($icon)) {
					$icon = NULL;
				}
				new GorgImage($game->name, $game->description, './games/'.$gameName.'/thumbnail.jpg', 'game.php?game='.$gameName, $icon);
			}
		}
	}
	
	include "footer.inc";
?>
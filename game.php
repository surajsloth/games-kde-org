<?php
	session_start();
	include_once("db_auth.inc");
	$gamename = $_GET['game'];
	if (!$gamename) {
		include 'header.inc';
		print 'Please Select a Game';
		include 'footer.inc';
		exit;
	}

	if (!file_exists('games/'.$gamename.'/info.xml')) {
		include 'header.inc';
		print 'Cannot Find the Specified Game';
		include 'footer.inc';
		exit;
	}
	$game = new simpleXmlElement(file_get_contents('games/'.$gamename.'/info.xml'));
	$game = $game->game;
	$page_title = $game->name." Information";
	include "header.inc";
	include('gamesHeader.php');
	?>
	<br />
	<div class="section">
		<h1>
		Description
		</h1>
		<div>
			<div style="float:left;margin-right: 2px;">
				<?php $icon = 'games/'.$gamename.'/icon.png'; ?>
				<img src="<?php print $icon; ?>" alt="" />
			</div>
	                 <?php
	                 	$desc = $game->description;
	                 	$desc = str_replace('[p]', '<p>', $desc);
	                 	$desc = str_replace('[/p]', '</p>', $desc);
	                 	$desc = str_replace('[b]', '<b>', $desc);
	                 	$desc = str_replace('[/b]', '</b>', $desc);
	                 	$desc = str_replace('[i]', '<i>', $desc);
	                 	$desc = str_replace('[/i]', '</i>', $desc);
	                 	$desc = str_replace('[u]', '<u>', $desc);
	                 	$desc = str_replace('[/u]', '</u>', $desc);
	                 	$desc = str_replace('[list]', '<ul>', $desc);
	                 	$desc = str_replace('[/list]', '</ul>', $desc);
	                 	$desc = str_replace('[item]', '<li>', $desc);
	                 	$desc = str_replace('[/item]', '</li>', $desc);
	                 	$desc = str_replace('[link]', '<a href=', $desc);
	                 	$desc = str_replace('[/link]', '</a>', $desc);
	                 	$desc = str_replace('[linktext]', '>', $desc);
	                 	$desc = str_replace('[/linktext]', '', $desc);
	                 	print $desc;
		       ?>
		</div>
		<br /><br />
	</div>

	<div class="section">
		<h1>
		Screenshots
		</h1>
		<?php
		$shots = 'games/'.$gamename.'/screenshots/thumbnails';
		if (is_dir($shots)) {
			$shots = scandir($shots);

			if (count($shots) > 1) {
				include_once('GorgImage.php');
				$out = '';
				foreach($shots as $shot) {
					$shotpath = 'games/'.$gamename.'/screenshots/thumbnails/'.$shot;
					if (is_file($shotpath)) {
						$size = getimagesize($shotpath);
						$origpath = 'games/'.$gamename.'/screenshots/originals/'.$shot;
						if (!file_exists($origpath)) {
							$origpath = NULL;
						}
						$out .="<a rel='lightbox' href='$origpath'><img src='$shotpath' alt='' width='$size[0]' height='$size[1]' /></a>&#160;&#160;";
					}
				}
				new BlueBox($out);
			}
		}
	?>
	<br /><br />
	</div>
	<?php
		if ( $game->howto) {
		?>


	<div class="section">
		<h1>
			Howto
		</h1>
		<?php
			$howto = $game->howto;
			$howto = str_replace('[p]', '<p>', $howto);
			$howto = str_replace('[/p]', '</p>', $howto);
			$howto = str_replace('[b]', '<b>', $howto);
			$howto = str_replace('[/b]', '</b>', $howto);
			$howto = str_replace('[i]', '<i>', $howto);
			$howto = str_replace('[/i]', '</i>', $howto);
			$howto = str_replace('[u]', '<u>', $howto);
			$howto = str_replace('[/u]', '</u>', $howto);
			$howto = str_replace('[list]', '<ul>', $howto);
			$howto = str_replace('[/list]', '</ul>', $howto);
			$howto = str_replace('[item]', '<li>', $howto);
			$howto = str_replace('[/item]', '</li>', $howto);
			$howto = str_replace('[link]', '<a href=', $howto);
			$howto = str_replace('[/link]', '</a>', $howto);
			$howto = str_replace('[linktext]', '>', $howto);
			$howto = str_replace('[/linktext]', '', $howto);
			print $howto;
		?>
		<br /><br />
	</div>

	<?php
	}
	?>
	<div class="section">
		<h1>
			Credits
		</h1>
		<?php
		$appinfo = new AppInfo($game->name);

		if (file_exists($icon)) {
			$size = getimagesize($icon);
			$appinfo->setIcon($icon, $size[0], $size[1]);
		}
		$appinfo->setVersion($game->version);
		$appinfo->setLicense(trim($game->credits->license));
		$appinfo->setCopyright($game->credits->year, $game->name." Team");

		if (count($game->credits->authors->author) > 0) {
			foreach($game->credits->authors->author as $author) {
				$appinfo->addAuthor($author->name, trim($author->email));
			}
		}

		if ( count($game->credits->contributors->contributor) > 0) {
			foreach($game->credits->contributors->contributor as $contributor) {
				$appinfo->addContributor($contributor->name, $contributor->email);
			}
		}
		$appinfo->show();
		?>
	</div>

	<?php
	include "footer.inc";
?>


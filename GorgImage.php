<?php
     
	class GorgImage {
		var $name;
		var $description;
		var $icon;
		var $image;
		var $link;
	
		function GorgImage($name, $description, $image=Null, $link=NULL, $icon=NULL) {
			$gal = file_get_contents('ImageGal.html');
			$size = getimagesize($image);

                        $desc = $description;
	                $desc = str_replace('[p]', '<p>', $desc);
	                $desc = str_replace('[/p]', '</p>', $desc);
	                $desc = str_replace('[b]', '<b>', $desc);
	                $desc = str_replace('[/b]', '</b>', $desc);
	                $desc = str_replace('[i]', '<i>', $desc);
	                $desc = str_replace('[/i]', '</i>', $desc);
	                $desc = str_replace('[u]', '<u>', $desc);
	                $desc = str_replace('[/u]', '</u>', $desc);
	                $desc = str_replace('[list]', '<ul>', $desc);
	                $desc = str_replace('[/list]', '</ul>', $desc);
	                $desc = str_replace('[item]', '<li>', $desc);
	                $desc = str_replace('[/item]', '</li>', $desc);
	                $desc = str_replace('[link]', '<a href=', $desc);
	                $desc = str_replace('[/link]', '</a>', $desc);
	                $desc = str_replace('[linktext]', '>', $desc);
	                $desc = str_replace('[/linktext]', '', $desc);
			
			$gal = str_replace('___NAME___', $name, $gal);
			$gal = str_replace('___HREF___', $link, $gal);
			$gal = str_replace('___DESCRIPTION___', $desc, $gal);
			$gal = str_replace('___ICON_SRC___', $icon, $gal);
			$gal = str_replace('___IMAGE_SRC___', $image, $gal);
			$gal = str_replace('___IMAGE_WIDTH___', $size[0], $gal);
			$gal = str_replace('___IMAGE_HEIGHT___', $size[1], $gal);
			new BlueBox($gal);
		}
	}

	class BlueBox {
		function BlueBox($html) {
			$box = file_get_contents('BlueBox.html');
			$out = str_replace('___HTML___', $html, $box);
			print $out;
		}
	}

<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KTron </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/ktron.png" ALT="KTron" BORDER="0" ALIGN="right" WIDTH="119" HEIGHT="95">
      Well known from the famous movie, <em>Tron</em> is a popular computer
      game for two players. In a fast action sequence both players have 
      to move and avoid colliding with any walls, the opponent as well as the
      own path. The player colliding first looses the game.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:matthias.kiefer@NO__SPAMgmx.de">Matthias Kiefer</a> 
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v1.0 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1-2 player <br> 
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>



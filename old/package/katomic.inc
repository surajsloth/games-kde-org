<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KAtomic </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/katomic.png" ALT="kAtomic" BORDER="0" ALIGN="right" WIDTH="121" HEIGHT="92">
        <em>KAtomic</em> is a thinking game where you have to form chemical
        molecules out of atoms. This is done with a nice graphical
        interface where you can move each atom in a labyrinth.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:AndreasWuest@NO__SPAMgmx.de">Andreas W&uuml;st</a> 
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v2.0 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>


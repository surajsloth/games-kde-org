<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KSnakerace </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/ksnakerace.png" ALT="KSnakerace" BORDER="0" ALIGN="right" WIDTH="102" HEIGHT="121">
      <em>KSnake Race</em> is a fast action game where you steer a snake
      which has to eat food. While eating the snake grows. But
      once a player collides with the other snake or the wall the
      game is lost. This becomes of course more and more difficult
      the longer the snakes grow.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:mfilippi@NO__SPAMsade.rhein-main.de">Michel Filippi</a> <br>
        Robert Williams <br>
        <a href="mailto:achant@NO__SPAMhome.com">Andrew Chant</a> 
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v0.3.1 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>


<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KPatience </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kpat.png" ALT="KPatience" BORDER="0" ALIGN="right" WIDTH="119" HEIGHT="85">
          <em>KPatience</em> is a collection of various patience games
          known all over the world. It includes <em>Klondike</em>,
          <em>Freecell</em>, <em>Yukon</em>, <em>Forty and Eight</em>
          and many more. The game has nice graphics and many different 
          carddecks. 
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        Paul Olav Tvete <br>
        <a href="mailto:mweilguni@NO__SPAMkde.org">Mario Weilguni</a> <br>
        <a href="mailto:ettrich@NO__SPAMkde.org">Matthias Ettrich</a> <br>
        <a href="mailto:barrett@NO__SPAMlabma.ufrj.br">Rodolfo Borges</a> <br>
        <a href="mailto:kpat@NO__SPAMincense.org">Peter H. Ruegg</a> <br> 
        <a href="mailto:koch@NO__SPAMkde.org.de">Michael Koch</a>  <br>
        <a href="mailto:mm@NO__SPAMcaldera.de">Marcus Meissner</a>  <br>
        <a href="mailto:shlomif@NO__SPAMvipe.technion.ac.il">Shlomi Fish</a> <br>
        <a href="mailto:coolo@NO__SPAMkde.org">Stephan Kulow</a> 
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v2.0 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>


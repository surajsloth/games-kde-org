<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KSame </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/ksame.png" ALT="KSame" BORDER="0" ALIGN="right" WIDTH="119" HEIGHT="99">
    <em>KSame</em> is a simple game played for the high score. 
    It has been inspired by <em>SameGame</em>, which is only really famous on
    the <em>Macintosh</em>. 
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        Marcus Kreutzberger<br>
        <a href="mailto:coolo@NO__SPAMkde.org">Stephan Kulow</a> 
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v0.5 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>


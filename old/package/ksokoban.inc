<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KSokoban </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/ksokoban.png" ALT="KSokoban" BORDER="0" ALIGN="right" WIDTH="119" HEIGHT="93">
      <em>KSokoban</em> is logic game where 
      a warehouse keeper trying to push crates to their proper locations
      in a warehouse.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:awl@NO__SPAMhem.passagen.se">Anders Widell</a> 
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v0.4.2 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>


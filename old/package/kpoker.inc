<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KPoker </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kpoker.png" ALT="KPoker" BORDER="0" ALIGN="right" WIDTH="121" HEIGHT="89">
      <em>KPoker</em> is a card game following the rules of the original
      <em>Poker</em> game. The game provides a computer player and various different carddecks.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:b_mann@NO__SPAMgmx.de">Andreas Beckermann</a> <br>
        <a href="mailto:whynot@NO__SPAMmabi.de">Jochen Tuchbreiter</a> 
    </td></tr>

    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
        <a href="http://kpoker.sourceforge.net">kpoker.sourceforge.net</a>
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v0.7 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1-2 player <br> 
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>


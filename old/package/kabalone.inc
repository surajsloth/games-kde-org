<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> Kenolaba </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kabalone.png" ALT="Kenolaba" BORDER="0" ALIGN="right" WIDTH="106" HEIGHT="120">
       <em>Kenolaba</em> is a two player tactical board game following
       the rules of the board game <em>Abalone</em>. In the game two players
       have to try to shove the opponents pieces from the game board.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:Josef.Weidendorfer@NO__SPAMin.tum.de">Josef Weidendorfer</a>
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v1.0.6a - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1-2 player <br> 
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
        &nbsp;
        <img src="./images/network.png" border="0"  height="24" width="24" alt="network game">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>


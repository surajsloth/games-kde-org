<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
  <h3>TaxiPilot</h3>
  <table><tr><td>
    <a href="images/taxipilot_big.jpg">
    <img src="images/taxipilot.jpg" ALT="taxipilot" BORDER="0" ALIGN="right" WIDTH="XXX" HEIGHT="XXX">
    </a>
    Taxipilot is a silly arcade-type game based on the simple but addictive 
    principle of the classic Spacetaxi-game: You're flying a taxi and have to
    pick up passengers waiting on a number of platforms, and drop them, where they 
    want to go.<br><br>
    While the graphics are not exactly of professional quality, there are only
    six different levels so far, and sound is still missing completely in the
    current version, the game does offer some nice surprises and nifty features, that 
    make it worth wasting some time.<br><br>
    The game comes with an extensive documentation on how to create your 
    own levels. So if you happen to be a better graphics designer than the 
    author, the latter would be happy to hear from you.<br>
    <br>
    (21/05/2002/<a href="mailto:mailto:tfry@NO__SPAMusers.sourceforge.net">TF</a>)
    <br>
   </td>
  </tr></table>
  <table>
    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
       <a href="http://taxipilot.sourceforge.net">http://taxipilot.sourceforge.net</a>
    </td></tr>
    <tr><td valign=top>
       Screenshots:
    </td><td valign=top>
       <a href="http://taxipilot.sourceforge.net/pics.php">http://taxipilot.sourceforge.net/pics.php</a>
    </td></tr>
    <tr><td valign=top>
       Contact:
    </td><td valign=top>
       <a href="mailto:tfry@NO__SPAMusers.sourceforge.net">Thomas Friedrichsmeier</a>
    </td></tr>
    <tr><td valign=top>
      License:
    </td><td valign=top>
      Code and Graphics are published under the GNU General Public License (GPL).
    </td></tr>
  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
  <h3> FreeForce </h3>
  <table><tr><td>
    <a href="images/freeforce_big.png">
    <img src="images/freeforce_small.png" ALT="FreeForce" BORDER="0" ALIGN="right" WIDTH="116" HEIGHT="83">
    </a>
   FreeForce is a real-time strategic game like Command&Conquer(tm). It has
   plugable units, a mapeditor and lot of other features. The game is still
   under development and is not playable yet. At this time you can test some
   features wich are already implemented in the game (little economy
   system). When you like to help us, join our projekt under <a
   href="mailto:join@freeforce.de">join@freeforce.de</a>.
   There you get your freeforce mail-adress, mailinglist acces and infos
   about developing freeforce.<br>
    (21/03/2002/<a href="mailto:albi@freeforce.de">AL</a>) 
   </td>
  </tr></table>
  <table>
    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
       <a href="http://www.freeforce.de">www.freeforce.de</a>
    </td></tr>
    <tr><td valign=top>
       Contact:
    </td><td valign=top>
       <a href="mailto:albi@freeforce.de">Albrecht Liebscher</a><br>
       <a href="mailto:dd@lst.de">Dennis Deitermann</a>
    </td></tr>
    <tr><td valign=top>
      License:
    </td><td valign=top>
      Code and Graphics are published under the GNU General Public License (GPL).
    </td></tr>
  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

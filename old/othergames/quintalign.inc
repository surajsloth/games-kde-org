<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> Quintalign </h3>

  <table ><tr><td>
    <p>
    <IMG SRC="images/quintalign.png" ALT="Quintalign" BORDER="0" ALIGN="right" WIDTH="129" HEIGHT="99"> 
    Quintalign is an intriguing boardgame a bit similar
    to other Teris like boardgames. 
    By moving pieces on a gameboard and aligning them
    you have to keep the board free,
    fighting against new pieces appearing every move and cluttering
    the board.
    The game extends the <em>Lines</em> and <em>Klines</em> games
    by variable and new rules and appealing graphics.
    The different levels make the game appropriate for
    everybody.
    </p>
    <p>

    (14/04/2002/<a href="mailto:martin@NO__SPAMheni-online.de">MH</a>)
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
      <a href="http://www.heni-online.de/linux/">
	 http://www.heni-online.de/linux/</a>
    </td></tr>

    <tr><td valign=top>
       Screenshots:
    </td><td valign=top>
      <a href="http://www.heni-online.de/linux/quinta2.html">
	 http://www.heni-online.de/linux/quinta2.html</a>
    </td></tr>

    <tr><td valign=top>
       Contact:
    </td><td valign=top>
       <a href="mailto:martin@NO__SPAMheni-online.de">M. Heni</a>
    </td></tr>

    <tr><td valign=top>
      License:
    </td><td valign=top>
      GNU General Public License (GPL).
    </td></tr>

    <tr><td valign=top>
      Version:
    </td><td valign=top>
       V0.61 (KDE 2.x), V1.0 (KDE3.x)
    </td></tr>

  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>


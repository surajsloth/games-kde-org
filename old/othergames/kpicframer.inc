		<!--  NEW SECTION -->
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0"
 CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0"
 BGCOLOR="#f0f0ff" ><tr><td>

    <h3> kPicframer </h3>

    <table> <tr> <td>
    <IMG SRC="images/kpicframer.png" ALT="kPicframer" BORDER="0" ALIGN="right" WIDTH="80" HEIGHT="82">
    KPicFramer is a small solitaire like card game, where you have to place the
 52 cards
    one by one into a 4x4 grid. All cards can be placed anywhere, except for the
 kings, the queens
    and the jacks.
    <p>
    After getting used to the game principle the game is fun to play. A nice
 addition to
    the patience games of KDE.
    <br>
    (15/12/2000/<a href="mailto:martin@NO__SPAMheni-online.de">MH</a>)
    <p>
    </td>
    </tr></table>

     <table>

     <tr>
     <td valign=top>
				Homepage:
     </td>
     <td valign=top>
        <A
 HREF="http://www.norbs.de/kpicframer/index.html">http://www.norbs.de/kpicframer
/index.html</A>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Screenshots:
     </td>
     <td valign=top>
         <A
 HREF="http://www.norbs.de/kpicframer/screenshots.html">http://www.norbs.de/kpic
framer/screenshots.html</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Contact:
     </td>
     <td valign=top>
         <a href="mailto:nandres@NO__SPAMgmx.de">Norbert Andres</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        License:
     </td>
     <td valign=top>
        Code and Graphics are published under the GNU General Public License
 (GPL).
     </td>
     </tr>
     </table>
     
      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>
		  

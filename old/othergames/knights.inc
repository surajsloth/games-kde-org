		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
    <h3> Knights </h3>

    <table> <tr> <td>
      <p>
    <IMG SRC="images/knights.png" ALT="Knights" BORDER="0" ALIGN="right" WIDTH="99" HEIGHT="120">
      <em>Knights</em> is a graphical users interface for <em>KDE</em> to chess engines
      like <em>GNUchess</em>.
      It also allows to connect to Internet chess servers. 
      Therefore, a game of chess can be played against the computer or against another
      human player. Of course to play against the computer a chess engine
      needs to be installed on your computer but they should be supplied with all
      Linux distributions. 
      </p>
      <p>
      The only backdraw at the moment is that you cannot select the strength
      of the computer enemy. But according to the auther this will be fixed
      in the next release.
      </p>
      <p>
      The program is really easy to use and provides most options you want
      from a chess GUI. 
      Moreover, the game excels with easy/auto configuration of the chess engines
      and many cool looking graphics themes. The graphics is definitely a
      highlight in this game.
      </p>
     <p>
      Knights can be strongly recommmended for all chess fans looking for a nice
      GUI interface.
      </p>
    <br>
    <p>
    (09/06/2002/<a href="mailto:martin@NO__SPAMheni-online.de">MH</a>)
    </p>
    </td>
    </tr></table>

     <table>

     <tr>
     <td valign=top>
				Homepage:
     </td>
     <td valign=top>
        <A HREF="http://knights.sourceforge.net">http://knights.sourceforge.net</A>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Screenshots:
     </td>
     <td valign=top>
         <A HREF="http://knights.sourceforge.net/screenshots.php">http://knights.sourceforge.net/screenshots.php</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Contact:
     </td>
     <td valign=top>
        <a href="mailto:tcorbin@NO__SPAMusers.sourceforge.net">Troy Corbin Jr.</a> 
     </td>
     </tr>

     <tr>
     <td valign=top>
        License:
     </td>
     <td valign=top>
        Code and Graphics are published under the GNU General Public License (GPL).
     </td>
     </tr>
     </table>
     
      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>
		  

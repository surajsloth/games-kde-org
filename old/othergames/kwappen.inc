<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KWappen </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kwappen.png" ALT="kWappen" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="130">
    Kwappen is a nice cardgame along the lines of
    other patience card games where the aim of the
    game is to order cards to remove them from the
    game board. In Kwappen this is reasonable easy
    once you got the idea and you mainly have to
    fight the time. Although this probably won't
    catch you for a really long time it is a very
    nice game for a coffee break. Especially, as
    it presents itself with a very nice and
    colorful graphical interface. The game uses
    German heraldry symbols as card deck. This
    is a novel and good looking and vey nice approach.
    </p>
    <p>
    As the main rules of the game a summarised
    on screen you will very quickly familiarise
    with the game.
    </p>
    <p>
    (14/03/2002/<a href="mailto:martin@NO__SPAMheni-online.de">MH</a>)
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
      <a href="http://www.lcs-chemie.de/kwappen_eng.htm">
	 http://www.lcs-chemie.de/kwappen_eng.htm</a>
    </td></tr>

    <tr><td valign=top>
       Screenshots:
    </td><td valign=top>
      <a href="http://www.lcs-chemie.de/kwappen_eng.htm">
	 http://www.lcs-chemie.de/kwappen_eng.htm</a>
    </td></tr>

    <tr><td valign=top>
       Contact:
    </td><td valign=top>
       <a href="mailto:jschulz-lcs@NO__SPAMt-online.de">Jens Schulz</a>
    </td></tr>

    <tr><td valign=top>
      License:
    </td><td valign=top>
      GNU General Public License (GPL).
    </td></tr>

    <tr><td valign=top>
      Version:
    </td><td valign=top>
       V1.0 (KDE 2.x)
    </td></tr>

  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>


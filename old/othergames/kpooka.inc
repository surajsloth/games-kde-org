		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
    <h3> kPooka </h3>

    <table> <tr> <td>
    <a href="images/kpooka.png">
    <IMG SRC="images/kpooka.png" ALT="kPooka" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="149">
    </a>
     Kpooka is a logic game which is a cross between Pengo and Sokoban.
     The basic idea is to move a red figure across the board to the exit. Of course
     it is not that easy. The way to the exit is blocked by green boxes, which you have
     to push away. Unfortunately, you lack the strength to push more than one box at the
     same time. So it ends up as an intellectual challenge what box to push.
     <p>
     The game has nice graphics, although some of the animations are not super smooth.
     However, kPooka is easily installed and played. You'll get the idea in a minute after
     starting the game. Like Sokoban it
     has an addictiveness to it which let you try a level until you manage to solve it.
     A well thought feature is that you do not have to play all levels again if you
     restart the game the next day but can continue with your highest level.
     <p>
     A few drawbacks can be found in the present version though: The game can
     only be played via keyboard and not mouse. There are not so many levels 
     available. As you can created your own levels, this fact is compensated
     though. 
     <p>
      This game is a nice distraction in a coffee break.
    <br>
    (27/06/2001/<a href="mailto:martin@NO__SPAMheni-online.de">MH</a>)
    <p>
    </td>
    </tr></table>

     <table>

     <tr>
     <td valign=top>
				Homepage:
     </td>
     <td valign=top>
        <A HREF="http://www.mrbogus.org.za/kpooka/">http://www.mrbogus.org.za/kpooka/</A>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Screenshots:
     </td>
     <td valign=top>
         <A HREF="http://www.mrbogus.org.za/kpooka/">http://www.mrbogus.org.za/kpooka/</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Contact:
     </td>
     <td valign=top>
         <a href="mailto:paulrahme@NO__SPAMyahoo.com">Paul Rahme</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        License:
     </td>
     <td valign=top>
        Code and Graphics are published under the GNU General Public License (GPL).
     </td>
     </tr>
     </table>
     
      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>
		  

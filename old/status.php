<?php
  $page_title = "KDE Games Package Status";
  include "games.inc";

  $author="M. Heni";
  $mail="martin@NO__SPAMheni-online.de";
  include "header.inc";
?>

<!-- MH: 03-2003: Page currently unused -->

		  <FONT SIZE="+1">T</FONT>his section contains the status and
      some  information about games
		  packaged or intended for packaging with KDE 2.1.
  <p>

  &nbsp;<p>
  &nbsp;<p>
		  

      <h2>Status of the kdegames package</h2>
  
      <FONT size="+1">D</FONT>evelopment status of the games in the <i>kdegames</i> packages.
      <p>
      <table border="1" bgcolor="#F0F0FF">
      <tr align="left">
        <th>Program</th>
        <th>Type</th>
        <th>Player</th>
        <th>Version</th>
        <th>Authors</th>
        <th>Description</th>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kAbalone</td>
      <td>
        Boardgame<br>
        <img src="./images/computer.png" border="0"  height="16" width="16" alt="computerplayer">
        <img src="./images/network.png" border="0"  height="16" width="16" alt="network game">
        </td>
      <td>1-2</td>
      <td bgcolor="#00FF00">v1.06a</td>
      <td nowrap>
        <a href="mailto:Josef.Weidendorfer@NO__SPAMin.tum.de">Josef Weidendorfer</a>
        <br>
        Robert Williams
        </td>
      <td >
          Abalone is a two player tactical board game.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kAsteroids</td>
      <td>
        Arcade<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v2.2</td>
      <td nowrap>
        <a href="mailto:mjones@NO__SPAMkde.org">Martin R. Jones</a> 
        </td>
      <td >
          Asteroids is a fast arcade shooting game where the
          player has to survive the passage of an asteroid field.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kAtomic</td>
      <td>
        Logic<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v2.0</td>
      <td nowrap>
        <a href="mailto:AndreasWuest@NO__SPAMgmx.de">Andreas W&uuml;st</a> 
        </td>
      <td >
          Atomic is a thinking game where you have to form chemical
          molecules out of atoms.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kBackgammon</td>
      <td>
        Boardgame<br>
        <img src="./images/computer.png" border="0"  height="16" width="16" alt="computerplayer">
        <img src="./images/network.png" border="0"  height="16" width="16" alt="network game">
        </td>
      <td>1-2</td>
      <td bgcolor="#00FF00">v1.9.9</td>
      <td nowrap>
        <a href="mailto:jens@NO__SPAMhoefkens.com">Jens Hoefkens</a><br>
        <a href="mailto:gobo@NO__SPAMimada.sdu.dk">Bo Thorsen</a>
        </td>
      <td >
          This is a graphical backgammon program. It supports 
          backgammon games with other players, games against
          computer engines like GNU bg and even
          on-line games.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kBlackbox</td>
      <td>
        Logic<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v0.3.0</td>
      <td nowrap>
        <a href="mailto:cimrman3@NO__SPAMstudents.zcu.cz">Robert Cimrman</a> 
        </td>
      <td >
          Blackbox is a graphical logical game, inspired by emacs' blackbox.
          It is a game of hide and seek played on an grid of boxes.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kFouleggs</td>
      <td>
        Arcade<br>
        <img src="./images/network.png" border="0"  height="16" width="16" alt="network game">
        <img src="./images/computer.png" border="0"  height="16" width="16" alt="computerplayer">
        </td>
      <td>1-2</td>
      <td bgcolor="#00FF00">v2.0.5</td>
      <td nowrap>
        Eirik Eng<br>
        <a href="mailto:hadacek@NO__SPAMkde.org">Nicolas Hadacek</a> 
        </td>
      <td >
          KFouleggs is a clone of the well-known (at least
          in Japan) PuyoPuyo game
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kjezz</td>
      <td>
        Action<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v0.1</td>
      <td nowrap>
        <a href="mailto:1Stein@NO__SPAMgmx.de">Stefan Schimanski</a><br>
        <a href="mailto:ssigala@NO__SPAMglobalnet.it">Sandro Sigala</a>
        </td>
      <td >
        Your task in Jazz Ball is to catch 
        several moving balls in a rectangular game field by building  walls.
        The motivation consists of finding new and advanced strategies to 
        catch as many balls as possible.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kJumpingCube</td>
      <td>
        Tactic<br>
        <img src="./images/computer.png" border="0"  height="16" width="16" alt="computerplayer">
        <img src="./images/network.png" border="0"  height="16" width="16" alt="network game">
        </td>
      <td>1-2</td>
      <td bgcolor="#00FF00">v1.0</td>
      <td nowrap>
        <a href="mailto:matthias.kiefer@NO__SPAMgmx.de">Matthias Kiefer</a>
        </td>
      <td >
        KJumpingCube is a tactical one- or two-player game.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>Konquest</td>
      <td>
        Strategy<br>
        <img src="./images/network.png" border="0"  height="16" width="16" alt="network game">
        </td>
      <td>n</td>
      <td bgcolor="#00FF00">v0.99.2</td>
      <td nowrap>
        <a href="mailto:rsteffen@NO__SPAMia.net">Russ Steffen</a> 
        </td>
      <td >
          This the KDE version of Gnu-Lactic Konquest, a multi-player
          strategy game. The goal of the game is to expand your interstellar
          empire across the galaxy and of course, crush your rivals in the
          process. 
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>Lieutenant sKat</td>
      <td>
        Cardgame<br>
        <img src="./images/computer.png" border="0"  height="16" width="16" alt="computerplayer">
        <img src="./images/network.png" border="0"  height="16" width="16" alt="network game">
        </td>
      <td>0-2</td>
      <td bgcolor="#00FF00">v0.91</td>
      <td nowrap>
        <a href="mailto:martin@NO__SPAMheni-online.de">Martin Heni</a> 
        </td>
      <td >
          Lieutenant sKat is a two player card game which follows
          the rules for the German game Skat. 
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kMahjongg</td>
      <td>
        Logic<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v0.7.2</td>
      <td nowrap>
        <a href="mailto:in5y158@NO__SPAMpublic.uni-hamburg.de">Mathias Mueller</a> 
        </td>
      <td >
          Mahjongg is a clone of the well known tile based patience game
          of the same name.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kMinesweeper</td>
      <td>
        Logic<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v2.0.11</td>
      <td nowrap>
        <a href="mailto:hadacek@NO__SPAMkde.org">Nicolas Hadacek</a><br> 
        Andreas Zehender
        </td>
      <td >
          Minesweeper is <em>the</em> classical game where you
          have to find mines by logical deduction.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kPatience</td>
      <td>
        Cardgame<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v2.0</td>
      <td nowrap>
        Paul Olav Tvete <br>
        <a href="mailto:mweilguni@NO__SPAMkde.org">Mario Weilguni</a> <br>
        <a href="mailto:ettrich@NO__SPAMkde.org">Matthias Ettrich</a> <br>
        <a href="mailto:barrett@NO__SPAMlabma.ufrj.br">Rodolfo Borges</a> <br>
        <a href="mailto:kpat@NO__SPAMincense.org">Peter H. Ruegg</a> 
        </td>
      <td >
          This is a collection of patience games.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kPoker</td>
      <td>
        Cardgame<br>
        <img src="./images/computer.png" border="0"  height="16" width="16" alt="computerplayer">
        </td>
      <td>1-2</td>
      <td bgcolor="#00FF00">v0.7</td>
      <td nowrap>
        <a href="mailto:b_mann@NO__SPAMgmx.de">Andreas Beckermann</a> <br>
        <a href="mailto:whynot@NO__SPAMmabi.de">Jochen Tuchbreiter</a> 
        </td>
      <td >
          Poker is a card game.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kReversi</td>
      <td>
        Boardgame<br>
        <img src="./images/computer.png" border="0"  height="16" width="16" alt="computerplayer">
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v1.2.1</td>
      <td nowrap>
        <a href="mailto:mweilguni@NO__SPAMsime.com">Mario Weilguni</a> 
        </td>
      <td >
          Reversi is a board game game.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kSame</td>
      <td>
        Logic<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v0.5</td>
      <td nowrap>
          Marcus Kreutzberger
        </td>
      <td >
          The Same Game is a logical thinking game.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kShisen-Sho</td>
      <td>
        Logic<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v1.3</td>
      <td nowrap>
        <a href="mailto:mweilguni@NO__SPAMsime.com">Mario Weilguni</a> 
        </td>
      <td >
        Shisen-Sho is similar to Mahjongg. The object of the game
        is to remove all tiles from the field.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kSirtet</td>
      <td>
        Arcade<br>
        <img src="./images/network.png" border="0"  height="16" width="16" alt="network game">
        </td>
      <td>1-2</td>
      <td bgcolor="#00FF00">v2.0.7</td>
      <td nowrap>
        Eirik Eng<br>
        <a href="mailto:hadacek@NO__SPAMkde.org">Nicolas Hadacek</a> 
        </td>
      <td >
          Sirtet is a clone of the Tetris family.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kSmiletris</td>
      <td>
        Arcade<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v2.0</td>
      <td nowrap>
        <a href="mailto:ssigala@NO__SPAMglobalnet.it">Sandro Sigala</a> 
        </td>
      <td >
          Smiletris is a clone of the Tetris family, but does not
          completely follow the usual Tetris games.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kSnakeRace</td>
      <td>
        Arcade<br>
        <img src="./images/computer.png" border="0"  height="16" width="16" alt="computerplayer">
        </td>
      <td>1-2</td>
      <td bgcolor="#00FF00">v0.3.1</td>
      <td nowrap>
        <a href="mailto:mfilippi@NO__SPAMsade.rhein-main.de">Michel Filippi</a> <br>
        Robert Williams <br>
        <a href="mailto:achant@NO__SPAMhome.com">Andrew Chant</a> 
        </td>
      <td >
          Snake Race is a fast action game where you steer a snake
          which has to eat food. While eating the snake grows. The
          player who collides first with something looses the game.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kSokoban</td>
      <td>
        Logic<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v0.4.0</td>
      <td nowrap>
        <a href="mailto:awl@NO__SPAMhem.passagen.se">Anders Widell</a> 
        </td>
      <td >
         Sokoban is logic game where 
         a warehouse keeper trying to push crates to their proper locations
         in a warehouse.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kSpaceduel</td>
      <td>
        Arcade<br>
        <img src="./images/computer.png" border="0"  height="16" width="16" alt="computerplayer">
        </td>
      <td>1-2</td>
      <td bgcolor="#00FF00">v1.0</td>
      <td nowrap>
        <a href="mailto:az@NO__SPAMazweb.de">Andreas Zehender</a> 
        </td>
      <td >
      Spaceduel is an space arcade game for two players.
      Each player controls a ship that flies around the sun
      and tries to shoot at the other ship.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kTron</td>
      <td>
        Arcade<br>
        <img src="./images/computer.png" border="0"  height="16" width="16" alt="computerplayer">
        </td>
      <td>1-2</td>
      <td bgcolor="#00FF00">v1.0</td>
      <td nowrap>
        <a href="mailto:matthias.kiefer@NO__SPAMgmx.de">Matthias Kiefer</a> 
        </td>
      <td >
          Tron is a fast action game where you must avoid collisions with
          obstacles and opponents.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kTuberling</td>
      <td>
        Children<br>
        </td>
      <td>1</td>
      <td bgcolor="#00FF00">v0.2.5</td>
      <td nowrap>
        <a href="mailto:ebisch@NO__SPAMcybercable.tm.fr">Eric Bischoff</a> <br>
        John Calhoun
        </td>
      <td >
         Tuberling is a children's game where you can build a potato man.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kwin4</td>
      <td>
        Boardgame<br>
        <img src="./images/computer.png" border="0"  height="16" width="16" alt="computerplayer">
        <img src="./images/network.png" border="0"  height="16" width="16" alt="network game">
        </td>
      <td>0-2</td>
      <td bgcolor="#00FF00">v0.91</td>
      <td nowrap>
        <a href="mailto:martin@NO__SPAMheni-online.de">Martin Heni</a> 
        </td>
      <td >
          Four wins is a two player board game which follows
          the rules for the Connect Four (TM) board game.
      </td>
      </tr>
      <!-- ******************************************************* -->
      </table>

      &nbsp;
      <p>

      <h2>Status of other KDE Games</h2>
   
      <FONT size="+1">D</FONT>evelopment status of the games planned to be in the <i>kdegames</i>
      package. They will most likely appear in the next official KDE release as part of the package.
      Please drop a mail on the mailing list
      <a href="mailto:kde-games-devel@kde.org">kde-games-devel@kde.org</a> if you have a game that
      you want to inculde into the next release and it can be discussed there.
      <p>
      <table border="1" bgcolor="#F0F0FF">
      <tr align="left">
        <th>Program</th>
        <th>Type</th>
        <th>Player</th>
        <th>Version</th>
        <th>Authors</th>
        <th>Description</th>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left">
      <td>kBattleship</td>
      <td>
        Boardgame<br>
        <img src="./images/network.png" border="0"  height="16" width="16" alt="network game">
        </td>
      <td>2</td>
      <td bgcolor="#00FF00">v0.5</td>
      <td nowrap>
        <a href="mailto:wildfox@NO__SPAMkde.org">Nikolas Zimmermann</a><br>
        <a href="mailto:molkentin@NO__SPAMkde.org">Daniel Molkentin</a>
        </td>
      <td >
        KBatteship is a KDE implentation of the
        popular game "Battleship" where you have to try to sink
        the opponents ships.
      </td>
      </tr>
      <!-- ******************************************************* -->
      </table>

      <p>
      &nbsp;
      <p>

      <table border="0">
      <tr align="left">
      <td colspan="2"> Legend:</td>
      <td>&nbsp;</td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td>&nbsp;</td>
      </tr>
      <tr align="left">
      <td height="30">&nbsp;</td>
      <td >
        <img src="./images/network.png" border="0" height="24" width="24" alt="network game">
      </td>
      <td>
          Network/multiplayer support
      </td>
      <td bgcolor="#00FF00">&nbsp;</td>
      <td>stable and working</td>
      </tr>
      <tr align="left">
      <td height="30">&nbsp;</td>
      <td >
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
      </td>
      <td>
          Computer player available
      </td>
      <td bgcolor="#FFC000">&nbsp;</td>
      <td>roughly playable</td>
      </tr>
      <tr align="left">
      <td height="30">&nbsp;</td>
      <td >
        &nbsp;
      </td>
      <td>
        &nbsp;
      </td>
      <td bgcolor="#FF0000">&nbsp;</td>
      <td>beta test</td>
      </tr>
      <tr align="left">
      <td height="30">&nbsp;</td>
      <td >
        &nbsp;
      </td>
      <td>
        &nbsp;
      </td>
      <td bgcolor="#0000EE">&nbsp;</td>
      <td>concept only</td>
      </tr>
      </table>


<?php
INCLUDE "nospam.inc";
?>

&nbsp;<p>     
&nbsp;<p>     
&nbsp;<p>     
<hr width=570 size=5 align="left" noshade>
<font size="-1">
	  Last update: 
    <?php echo (date("dS F Y",getlastmod()).", <a href=\"mailto:".$mail."\">".$author."</a>\n"); ?> 
</font>

<?php
INCLUDE "footer.inc";
?>

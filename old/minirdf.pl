#!/usr/bin/perl

# Perl script written by M. Heni <linux@heni-online.de>
# (c) September 2000
#
# Licences: GPL, description see KDE GPL textfile
#
#
# Usage: Start without arguments for default usage, i.e
#        processing of the file games-rated.rdf
#        Given a filename uses this RDF file instead
#        Given an URL, starting with http:// tries to
#        "wget" the file from there. If "wget" is not
#        Available you have to manually download the file
#
#        RDF location:
#        http://apps.kde.com/news/games-rated.rdf
#
#

$MAXRATING = 10;     # Maximum rating
$CODE="@@@";

#  Get the time
  $now=localtime(time);

   $now =~ s/Jan/January/;
   $now =~ s/Feb/February/;
   $now =~ s/Mar/March/;
   $now =~ s/Apr/April/;
   $now =~ s/May/May/;
   $now =~ s/Jun/June/;
   $now =~ s/Jul/July/;
   $now =~ s/Aug/August/;
   $now =~ s/Sep/September/;
   $now =~ s/Oct/October/;
   $now =~ s/Nov/November/;
   $now =~ s/Dec/December/;

  @Zeit = split(/ +/,$now);
  @Uhrzeit = split(/:/,$Zeit[3]);
  $now=$Zeit[2].". ".$Zeit[1]." ".$Zeit[4];


  # Default file
  $file="games-rated.rdf";
  if ($#ARGV>=0)
  {
    $file=$ARGV[0];
    # If an url with http:// is given we try to wget the
    # file. In case this does not work we revert to a
    # local copy
    if ($file =~ /http:\/\/([^\n]*)/i)
    {
      my $cmd="wget -t25 -q $file";
      print "Downloaded file $file...\n";
      my @tmp=split(/\//,$file);
      $file=$tmp[-1];
      rename $file,"$file.bak";
      my $result=(system $cmd)/256;
      if (!$result)
      {
        print "Download to $file  ok...\n";
        unlink "$file.bak";
      }
      else
      {
        print "Warning: Could not download $file.\nUsing local file if available.\n";
        rename "$file.bak",$file;
      }
      my @tmp=split(/\//,$file);
      $file=$tmp[-1];
    }
  }

  # open and read all necessary files
  print "Processing $file...\n";
  open (RDFFILE, "<$file") or die ("Cannot open '$file'\n");
  @rdfraw=<RDFFILE>;
  $rdfstr=join("",@rdfraw);
  close (RDFFILE);

  $output="<!-- Automatically generated list -->";

  $output.="<TABLE align=\"center\" width=\"90%\" border=\"0\"><tr><td>\n";

  @result=ParseRDFKey($rdfstr,"item");
  $count=1;
  foreach(@result)
  {
    my @tmp=();
    @tmp=ParseRDFKey($_,"link");
    $homepage=0;
    if ($#tmp>=0)
    {
      $homepage=$tmp[0];
    }

    @tmp=ParseRDFKey($_,"title");
    $title=0;
    if ($#tmp>=0)
    {
      $title=$tmp[0];
      if ($title =~ /APPS.KDE.com:[ ]*([^\n]*)/i)
      {
        $title=$1;
      }
    }

    @tmp=ParseRDFKey($_,"description");
    $description=0;
    if ($#tmp>=0)
    {
      $description=$tmp[0];
    }

    @tmp=ParseRDFKey($_,"icon");
    $icon=0;
		$iconwidth=32;
		$iconheight=32;
    if ($#tmp>=0)
    {
      my @tmp1=ParseRDFKey($tmp[0],"url");
      my @tmp2=ParseRDFKey($tmp[0],"width");
      my @tmp3=ParseRDFKey($tmp[0],"height");
			if ($#tmp1>=0)
			{
			  $icon=$tmp1[0];
				if ($#tmp2>=0)
				{
				  $iconwidth=$tmp2[0];
				}
				if ($#tmp3>=0)
				{
				  $iconheight=$tmp3[0];
				}
			}
    }

    @tmp=ParseRDFKey($_,"screenshots");
    $screenshot=0;
    if ($#tmp>=0)
    {
      my @tmp1=ParseRDFKey($tmp[0],"url");
			if ($#tmp1>=0)
			{
			  $screenshot=join($CODE,@tmp1);
			}
    }

    @tmp=ParseRDFKey($_,"rating");
    $rating=0;
    if ($#tmp>=0)
    {
      $rating=$tmp[0];
    }

    @tmp=ParseRDFKey($_,"authors");
    $author=0;
    $email=0;
    if ($#tmp>=0)
    {
      my @tmp1=ParseRDFKey($tmp[0],"name");
      my @tmp2=ParseRDFKey($tmp[0],"email");
			if ($#tmp1>=0)
			{
			  $author=join($CODE,@tmp1); 
			}
			if ($#tmp1>=0)
			{
			  $email=join($CODE,@tmp2); 
			}
    }

    $license=0;

    if ($title)
    {
      $output.=CreateHTML($title,$description,$homepage,$icon,$iconwidth,$iconheight,
			                    $rating,$author,$email,$screenshot,$license,$count);
      $count++;
    }
  } 
  $output.="<p>\nTop 10 list generated on $now\n<p>\n";
  $output.="</TD></TR></TABLE>\n";
  $output.="<!-- End automatically generated list -->";

  open (RDFFILE, ">top10rdf.raw") or die ("Cannot open 'top10rdf.raw'\n");
  print RDFFILE $output;
  close (RDFFILE);
  print "top10rdf.raw written...\n";

  exit;
  
# ------------- Subroutines ------------------
# Parse the RDF key pair (2nd argument) in a given string (1st argument)
sub ParseRDFKey
{
  my $str=$_[0];
  my $search=$_[1];
  my @result=();
  my $item=0;
  do
  {
    if ($str =~ /<$search>/i)
    {
      if ($' =~ /<\/$search>/i)
      {
        $item=$`;
        $str=$';
      }
      else
      {
        $item=0;
        $str=$';
      }
    }
    else
    {
      $str=0;
      $item=0;
    }
    if ($item)
    {
      push(@result,$item);
#      print "adding <$item>\n";
    }
  }while($str);
  return @result;
}

# Create a HTML table from the given data
# CreateHTML($title,$description,$homepage,$icon,$iconwidth,$iconheight,
#            $rating,$author,$email,$screenshot,$license,$count);
sub CreateHTML
{
  my $str="";         
  my $title=$_[0];     
  my $description=$_[1];
  my $homepage=$_[2];  
  my $icon=$_[3];      
  my $iconwidth=$_[4]; 
  my $iconheight=$_[5]; 
  my $rating=$_[6];
  my $author=$_[7];    
  my $email=$_[8];     
  my $screenshot=$_[9];
  my $license=$_[10];
  my $count=$_[11];    

  $str.="<!-- NEW SECTION -->\n";
  $str.="<TABLE align=\"center\" width=\"100%\" bgcolor=\"#5555FF\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td>\n";
  $str.="<TABLE width=\"100%\" cellspacing=\"1\" cellpadding=\"3\" border=\"0\" bgcolor=\"#F0F0FF\" ><tr><td>\n";
  $str.="<FONT size=\"+1\">\n";
  $str.="<CENTER><B>Number $count</B><HR width=\"50%\"> </CENTER><P>\n";
  $str.="</FONT>\n";
  $str.="<TABLE border=\"0\" bgcolor=\"#F0F0FF\">\n<TR><TD>\n";
  if (!$icon)
  {
    $icon="images/defaulticon.gif";
    $iconwidth=32;
    $iconheight=32;
  }
  if ($homepage)
  {
    $str.="<A HREF=\"$homepage\">";
  }
  $str.="<IMG SRC=\"$icon\" border=0 width=\"$iconwidth\" height=\"$iconheight\" alt=\"$title\" align=\"left\">\n";
  if ($homepage)
  {
    $str.="</A>";
  }
  $str.="</TD><TD>\n";
  $str.="&nbsp;&nbsp;<FONT size=\"+2\">\n";
  $str.="<B>$title</B>\n";
  $str.="</FONT>\n";
  $str.="</TD></TR>\n<TR><TD>&nbsp;</TD><TD>\n";
  $str.="&nbsp;&nbsp;";

  my $rate=100*$rating/$MAXRATING;
  my $rate5=5*int(int(10*$rating/2.0+0.5)/5.0+0.5);
  my $rateicon="http://apps.kde.com/img/stars/star".$rate5.".gif";
  if ($rate5>=10)
  {
    $str.="<IMG SRC=\"$rateicon\" width=\"90\" height=\"18\" border=0 alt=\"Rating: $rate%\" align=\"top\">\n";
  }
  $str.="</TD></TR></TABLE>\n";
  $str.="<P>\n";
  if ($description)
  {
    $str.=$description;
  }
  else
  {
    $str.="No description available.\n";
  }
  $str.="<P>\n";

  $str.="<TABLE>\n";

  # Author, Email
	my $authorstr="";
	my $authorhead="Author:";
	if ($author)
	{
	  my @tmp1=split(/$CODE/,$author);
		my @tmp2=split(/$CODE/,$email);
		# More than one author
		if ($#tmp1>0)
		{
	    $authorhead="Authors:";
		}
		my $i;
		for ($i=0;$i<=$#tmp1;$i++)
		{
			# Email exists
			if ($#tmp2>=$i)
			{
		    $authorstr.="<A HREF=\"mailto:$tmp2[$i]\">$tmp1[$i]</A><br>";
			}
			else
			{
		    $authorstr.="$tmp1[$i]<br>";
			}
		}
	}
	else
  {
    $authorstr="unknown";
  }
  $str.="<TR>\n";
  $str.="<TD valign=\"top\"><B>$authorhead</B></TD>\n";
  if ($email)
  {
    $str.="<TD valign=\"top\">$authorstr</TD>\n";
  }
  else
  {
    $str.="<TD valign=\"top\"> $author</TD>\n";
  }
  $str.="</TR>\n";

  # Homepage
  $str.="<TR>\n";
  $str.="<TD valign=\"top\"><B>Homepage:</B></TD>\n";
  if ($homepage)
  {
    $str.="<TD valign=\"top\"> <A HREF=\"$homepage\">$homepage</A></TD>\n";
  }
  else
  {
    $str.="<TD valign=\"top\">not available</TD>\n";
  }
  $str.="</TR>\n";

  # Screenshot
	my @tmp3=split(/$CODE/,$screenshot);
  if (!$screenshot)
  {
    @tmp3=();
  }
  
  $str.="<TR>\n";
  if ($#tmp3>0)
  {
    $str.="<TD valign=\"top\"><B>Screenshots:</B></TD>\n";
  }
  else
  {
    $str.="<TD valign=\"top\"><B>Screenshot:</B></TD>\n";
  }
  if ($#tmp3>=0)
  {
		my $i;
    $str.="<TD valign=\"top\">\n";
		for ($i=0;$i<=$#tmp3;$i++)
		{
		   $str.="<A HREF=\"$tmp3[$i]\">$tmp3[$i]</A><br>";
		}
    $str.="</TD>\n";
  }
  else
  {
    $str.="<TD valign=\"top\">not available</TD>\n";
  }
  $str.="</TR>\n";

  # Rating
  my $rate=100*$rating/$MAXRATING;
  $str.="<TR>\n";
  $str.="<TD valign=\"top\"><B>Rating:</B></TD>\n";
  $str.="<TD valign=\"top\"> $rate% </TD>\n";
  $str.="</TR>\n";

  $str.="</TABLE>\n";

  
  $str.="</TD></TR></TABLE>\n";
  $str.="</TD></TR></TABLE>\n";
  $str.="&nbsp;<BR>\n";
  return $str;
}




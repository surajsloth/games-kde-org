<!--[if gt IE 6]><!-->
  <link rel="stylesheet" type="text/css" href="style.css" />
<!--<![endif]-->

<!--[if !IE]>-->
  <script src="/media/javascripts/lightbox.js"></script>
<!--<![endif]-->

<!--[if lt IE 7.]>
<link rel="stylesheet" type="text/css" href="ie6_hacks.css" />
<script defer type="text/javascript" src="pngfix.js"></script>
<![endif]-->
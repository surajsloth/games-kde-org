<?php
  $site_title = "The KDE Games Center";
  $name = "games.kde.org";
  $site_menus = 1;
  $templatepath = "newlayout/";
  $site = 'games';
  $author="Emil Sedgh";
  $mail="emilsedgh@gmail.com";
  $showedit = False;
  $piwikSiteID = 9;
  $piwikEnabled = true;
  $site_showkdeevdonatebutton = true;
?>

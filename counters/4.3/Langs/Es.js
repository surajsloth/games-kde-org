Langs['4.3/Counter']['Banner'] = Array('','Configuración sencilla e intuitiva','Administración de archivos mejor que nunca','Visualización de fotos e imágenes hecha fácil','El poder de la web en la punta de tus dedos','Búsqueda y lanzador de aplicaciones mejorados','Soluciones de comunicación fantásticas','Experiencia de escritorio como nunca antes','Más de 60.000 actualizaciones','Más de 2.000 nuevas características');
Langs['4.3/Counter']['Square'] = Array('','Configurabilidad','Administración de archivos','Visualización de imágenes','Navegación Web2.0','Menús mejorados','Comunicaciones','Mejor escritorio','60.000 actualizaciones','2.000 nuevas características');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'La comunidad de KDE publica KDE 4.3 en %s días';
	Langs['4.3/Counter']['Square'][0]  = '%s días restantes';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 será publicado hoy';
	Langs['4.3/Counter']['Square'][0]  = 'Día de la publicación';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 publicado';
	Langs['4.3/Counter']['Square'][0]  = '4.3 publicado';
}
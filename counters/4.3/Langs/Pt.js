Langs['4.3/Counter']['Banner'] = Array('','Configuração simples e intuitiva','Gerenciamento de arquivos melhorado','Exibição de fotos e imagens facilitada','O poder da web nas pontas do seus dedos','Lançador de aplicativos e busca aprimorados','Soluções de comunicação fantásticas','A experiência do usuário como nunca se viu antes','Mais de 60.000 atualizações','Mais de 2.000 funcionalidades');
Langs['4.3/Counter']['Square'] = Array('','Configurabilidade','File management','Gerenciamento de arquivos','Exibição de imagens','Navegação Web 2.0','Menus aprimorados','Comunicações','Desktop melhorado','60.000 atualizações','2.000 novas funcionalidades');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'A comunidade do KDE lançará o KDE 4.3 em %s dias';
	Langs['4.3/Counter']['Square'][0]  = 'restam %s dias';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 será lançado hoje';
	Langs['4.3/Counter']['Square'][0]  = 'Dia do lançamento';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 lançado';
	Langs['4.3/Counter']['Square'][0]  = '4.3 lançado';
}
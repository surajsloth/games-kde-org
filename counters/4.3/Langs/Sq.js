Langs['4.3/Counter']['Banner'] = Array('','Konfigurim i thjeshtuar dhe intuitiv','Menaxhim fajlash më i mirë se kurrë','Foto dhe paraqitje imazhesh në mënyrë të thjeshtë','Fuqia e webit në gishtat tuaja','Kërkim dhe nisje programesh i avancuar','Mënyra komunikimi të përkryera','Eksperiencë desktopi si kurrë më parë','Mbi 60,000 azhornime','Mbi 2,000 funksionalitete të reja');
Langs['4.3/Counter']['Square'] = Array('','Konfigurim','Menaxhim fajlash','Paraqitje imazhesh','Lundrim Web2.0','Menu të përmirsuara','Komunikim','Desktop i përmirsuar','60,000 azhornime','2,000 funksionalitete të reja');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'Komuniteti i KDE do nxjerri KDE 4.3 mbas $s ditësh';
	Langs['4.3/Counter']['Square'][0]  = 'mungojnë %s ditë';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 do dali sot';
	Langs['4.3/Counter']['Square'][0]  = 'Dita e daljes';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 doli';
	Langs['4.3/Counter']['Square'][0]  = '4.3 doli';
}
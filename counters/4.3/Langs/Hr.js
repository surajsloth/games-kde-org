Langs['4.3/Counter']['Banner'] = Array('','Jednostavno i intuitivno podešavanje','Do sada najbolje upravljanje datotekama','Pregledavanje fotografija i slika postalo je jednostavno','Moć weba na vašem dlanu','Poboljšano pretraživanje i pokretanje aplikacija','Izvanredna komunikacijska rješenja','Iskustvo na radnoj površini kao nikada do sada','Više od 60.000 nadopuna','Više od 2.000 novih mogućnosti');
Langs['4.3/Counter']['Square'] = Array('','Podesivost','Upravljanje datotekama','Pregledavanje slika','Surfanje webom 2.0','Poboljšani izbornici','Komuniciranje','Bolje radno okruženje','60.000 nadopuna','2.000 novih mogućnosti');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'Zajednica KDE izdaje KDE 4.3 za %s dana';
	Langs['4.3/Counter']['Square'][0]  = 'Ostalo %s dana';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'Danas će biti izdan KDE 4.3';
	Langs['4.3/Counter']['Square'][0]  = 'Dan izdanja';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'Izašao je KDE 4.3';
	Langs['4.3/Counter']['Square'][0]  = 'Izašao je 4.3';
}
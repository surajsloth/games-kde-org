Langs['4.3/Counter']['Banner'] = Array('','Простая и интуитивная настройка','Работа с файлами стала лучше и быстрее','Просмотр фотографий и картинок теперь проще простого','С KDE весь Интернет в ваших руках','Упрощённый поиск и запуск приложений','Новые возможности сообщения и почты','Рабочий стол о котором вы всегда мечтали','Более 60,000 обновлений','Более 2,000 новых возможностей');
Langs['4.3/Counter']['Square'] = Array('','Настраиваемость','Работа с фйлами','Просмотр фото','Web2.0 Интернет','Упрощенные меню','Сообщение','Новые возможности','60,000 обновлений','2,000 новшеств');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'Общество KDE выпускает KDE 4.3 через %s дней';
	Langs['4.3/Counter']['Square'][0]  = 'Осталось %s дней';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'Сегодня выпуск KDE 4.3';
	Langs['4.3/Counter']['Square'][0]  = 'День выпуска';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'Состоялся выпуск KDE 4.3';
	Langs['4.3/Counter']['Square'][0]  = '4.3 уже вышел';
}
Langs['4.3/Counter']['Banner'] = Array('','Simpele en intuitieve instellingen','Bestandsbeheer beter dan ooit',"Foto's en figuren bekijken makkelijker gemaakt",'De kracht van het internet aan je vingertoppen','Verbeterde zoekfunctie en programmastarter','Geweldige communicatieoplossingen','Bureaubladervaring zoals nooit tevoren','Meer dan 60.000 verbeteringen','Meer dan 2.000 compleet nieuwe mogelijkheden');
Langs['4.3/Counter']['Square'] = Array('','Configureerbaar','Bestandsbeheerder','Figuren bekijken','Web2.0 browsen',"Verbeterde menu's",'Communicaties','Beter bureaublad','60.000 verbeteringen','2.000 nieuwe mogelijkheden');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE-gemeenschap geeft KDE 4.3 in %s dagen uit';
	Langs['4.3/Counter']['Square'][0]  = '%s dagen te gaan';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 zal vandaag worden vrijgegeven';
	Langs['4.3/Counter']['Square'][0]  = 'Vandaag uit';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 is vrijgegeven';
	Langs['4.3/Counter']['Square'][0]  = '4.3 is vrijgegeven';
}
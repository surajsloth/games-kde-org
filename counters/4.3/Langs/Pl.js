Langs['4.3/Counter']['Banner'] = Array('','Prosta i intuicyjna konfiguracja','Lepsze niż kiedykolwiek zarządzanie plikami','Ułatwione przeglądanie zdjęć i obrazków','Potęga Sieci w zasięgu ręki','Ulepszone wyszukiwanie i uruchamianie aplikacji','Fantastyczne rozwiązania komunikacyjne','Pulpit jakiego jeszcze nie widziałeś','Ponad 60000 aktualizacji','Ponad 2000 nowych możliwości');
Langs['4.3/Counter']['Square'] = Array('','Konfigurowalność','Zarządzanie plikami','Przeglądanie obrazków','Używanie Web2.0','Ulepszone menu','Komunikacja','Lepszy pulpit','60000 aktualizacji','2000 nowych możliwości');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'Społeczność KDE udostępni KDE 4.3 za %s dni';
	Langs['4.3/Counter']['Square'][0]  = 'Pozostało %s dni';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 zostanie udostępnione dzisiaj';
	Langs['4.3/Counter']['Square'][0]  = 'Dzień premiery';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 jest już dostępne';
	Langs['4.3/Counter']['Square'][0]  = '4.3 jest już dostępne';
}
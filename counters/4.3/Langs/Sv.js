Langs['4.3/Counter']['Banner'] = Array('','Simpel och intuitiv konfiguration','Bättre filhantering än någonsin','Förenklad Foto- och Bildvisning','Kraften hos Internet i dina fingertoppar','Förbättrade sök- och applikationsstartare','Fantastiska kommunikationslösningar','En skrivbordsupplevelse som aldrig förrut','Mer än 60,000 uppdateringar','Mer än 2,000 nya funktioner');
Langs['4.3/Counter']['Square'] = Array('','Konfigurerbart','Filhantering','Bildvisning','Web2.0 Surfning','Förbättrade Menyer','Kommunikationer','Bättre Skrivbord','60,000 uppdateringar','2,000 nya funktioner');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE communityn lanserar KDE 4.3 om %s dagar';
	Langs['4.3/Counter']['Square'][0]  = '%s dagar kvar';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 lanseras idag';
	Langs['4.3/Counter']['Square'][0]  = 'Lanserings Dag';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 - ute nu!';
	Langs['4.3/Counter']['Square'][0]  = '4.3 är Lanserat';
}
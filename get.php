<?php
  include_once( 'header.inc' );
  include_once( 'gamesHeader.php' );
  include_once( './GorgImage.php' );

  $distros[] = 'Ubuntu';
  $distros[] = 'Debian';
  $distros[] = 'Suse';
  $distros[] = 'Fedora';
  $distros[] = 'Gentoo';
  $distros[] = 'Mandriva';
  $distros[] = 'PCLinuxOS';

  asort( $distros );

  $platforms[] = 'Linux';
  $platforms[] = 'BSD';
  $platforms[] = 'Windows';
  $platforms[] = 'Source';

  function isPlatform( $platform ) {
    if ( stristr( $_SERVER['HTTP_USER_AGENT'], $platform ) ) {
      return true;
    }
  }

  function showPlatform( $platform, $special=False ) {
    $file = './platforms/'.$platform.'.php';
    if ( file_exists( $file ) ) {
      $logo = './pics/platforms/'.$platform.'.png';
      if ( file_exists( $logo ) ) {
	$logo = '<img src="'.$logo.'" alt="'.$platform.'" ><br />';
      } else {
	$logo = '';
      }
      if ( $special ) {
	$instructions = include_once( $file );
      } else {
	$instructions = file_get_contents( $file );
      }
      new BlueBox( $logo.'<h3>Instructions to install KDE Games using '.$platform.':</h3><p>'.$instructions.'</p>');
      return True;
    } else {
      return False;
    }
  }

  function searchThroughPlatforms( $list, $special=False ) {
    foreach( $list as $platform ) {
      if ( isPlatform( $platform ) ) {
	if (showPlatform( $platform, $special ) ) {
	  return True;
	} else {
	  return False;
	}
      }
    }
    return False;
  }

  if ( $_GET['platform'] ) {
    $found = False;
    foreach( $platforms as $platform ) {
      if ( $platform == $_GET['platform'] ) {
	showPlatform( $_GET['platform'], True );
	$found = True;
      }
    }

    foreach( $distros as $distro ) {
      if ( $distro == $_GET['platform'] ) {
	showPlatform( $_GET['platform'] );
	$found = True;
      }
    }
    if ( !$found ) {
      New BlueBox( 'We couldn\'t find the selected platform' );
    }
  } else {
      $found = searchThroughPlatforms( $distros );
      if ( !$found ) {
	searchThroughPlatforms( $platforms, True );
      }
  }

  ?>
    <h1>Please Select Another Operating System:</h1>
    <form action='./get.php' method='get'>
      <select name='platform'>
        <option value=''>Autodetect</option>
        <optgroup label='GNU/Linux Distro'>
          <?php
            foreach ( $distros as $distro ) {
              $default = '';
              if ( $_GET['platform'] == $distro ) {
                $default = 'selected="selected"';
              }
              print "<option value='".$distro."' $default>$distro</option>";
            }
          ?>
        </optgroup>
        <?php
          foreach ( $platforms as $platform ) {
            $default = '';
            if ( isset($_GET['platform']) && $_GET['platform'] == $platform ) {
              $default = 'selected="selected"';
            }
            print "<option value='".$platform."' $default>$platform</option>";
          }
        ?>
      </select>
      <input type='submit' />
    </form>
  <?php

  include_once( 'footer.inc' );
?>
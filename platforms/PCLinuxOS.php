<ul><li>open <b>Synaptic</b> package manager</li>
<li>go to Settings -&gt; Repositories</li>
<li>click on your main mirror and add kde4 to 'Section(s)'</li>
<li>press okay, then hit reload</li>
<li>now <b>KDE4</b> packages appear in the selection list</li>
<li>select 'kdegames' from the list and proceed to install it onto your system</li></ul>
<i><b>Please note:</b> it is highly recommended to install 'task-kde4' package first.</i>

KDE 4 Packages are now in Squeeze (currently Testing). You should have a suitable repository in your sources.list file. So please open <i>/etc/apt/sources.list</i> file and add the following line to it:
<br /><br />
<i>
  deb http://ftp.debian.org/debian/ squeeze main
</i>
<br /><br />
Now you could simply get the games by installing the <i>kdegames</i> package.
<br />
This could be done using Synaptic or with the following command:
<br /><br />
<i>
  aptitude install kdegames
</i>
<br /><br />
Note: Squeeze is a development branch and needs a bit of experience to be used.

<?php
$out='<ul>';
$out.='   <li>Please note that Windows version of KDE4 is highly experimental!';
$out.='      <br /><br /></li>';
$out.='   <li>Please follow these simple steps to install <b>KDEGames</b> onto windows platform:</li>';
$out.='</ul>';
$out.='<ol>';
$out.='    <li>Download KDE Windows Installer using the following link:<br />';
$out.='	<a  href="http://www.winkde.org/pub/kde/ports/win32/installer/kdewin-installer-gui-latest.exe">Get KdeGames for Windows.</a>';
$out.='    </li>';
$out.='    <li>Once the download is finished execute the Installer application and follow these instructions:<br />';
$out.='	<a href="http://www.winkde.org/pub/kde/ports/win32/installer/manual/index.html">KDE Windows installer instructions</a>';
$out.='    </li>';
$out.='    <li>Do not forget to checkmark <b>kdegames</b> on the package selection screen.';
$out.='    </li>';
$out.='</ol>';
return $out;
?>

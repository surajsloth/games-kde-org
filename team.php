<?php
	include 'header.inc';
	include('gamesHeader.php');
	
	$dirs = scandir('games');
	$page_title = 'The Team';
	
	if (is_array($dirs)) {
		foreach($dirs as $dir) {
			$infoFile = './games/'.$dir.'/info.xml';
			if (file_exists($infoFile)) {
				$game = new simpleXmlElement( file_get_contents( $infoFile ) );
				$game = $game->game;

				foreach($game->credits->authors->author as $author) {
					if ($author['maintainer'] || $author['artist']) {
						$authors[trim($author->name)]['games'][] = $game;
						$authors[trim($author->name)]['info'] = $author;
						if ($author['maintainer']) {
							$authors[trim($author->name)]['maintainer'] = True;
						}
						if ($author['artist']) {
							$authors[trim($author->name)]['artist'] = True;
						}
						if ($author['documentor']) {
							$authors[trim($author->name)]['documentor'] = True;
						}
					}
				}

				if ( count( $game->credits->contributors->contributor ) ) {
					foreach($game->credits->contributors->contributor as $author) {
						if ($author['maintainer'] || $author['artist'] || $author['documentor']) {
							$authors[trim($author->name)]['games'][] = $game;
							$authors[trim($author->name)]['info'] = $author;
							if ($author['maintainer']) {
								$authors[trim($author->name)]['maintainer'] = True;
							}
							if ($author['artist']) {
								$authors[trim($author->name)]['artist'] = True;
							}
							if ($author['documentor']) {
								$authors[trim($author->name)]['documentor'] = True;
							}
						}
					}
				}
			}
		}
	}
	
	?>

	<img src='./pics/icons/artist.png' alt='Artist' /> Artist
	<br />
	<img src='./pics/icons/documentor.png' alt='Documentor' /> Documentor
	<br />
	<img src='./pics/icons/maintainer.png' alt='Maintainer' /> Maintainer
	<br />
	<img src='./pics/icons/mail.png' alt='Send An Email' /> Send An Email
	<br /><br />

	<?php
	
	ksort($authors, SORT_STRING);
	if (is_array($authors)) {
		foreach($authors as $author) {
			print '<h1>';
			print $author['info']->name;
			print '</h1>';
	
			$image = './pics/authors/'.md5(trim($author['info']->name)).'.png';
			if (file_exists($image)) {
				print '<img src='.$image.' alt='.trim($author['info']->name).' />&#160;';
			}
			print "<a href='mailto: ".$author['info']->email."'>";
				print '<img src="pics/icons/mail.png" alt="Email" />&#160;';
			print "</a>";
		
			print '<br />';
			if ($author['maintainer']) {
				print '<img src="pics/icons/maintainer.png" alt="Maintainer" />&#160;';
			}
			if ($author['artist']) {
				print '<img src="pics/icons/artist.png" alt="Artist" />&#160;';
			}
			if ($author['documentor']) {
				print '<img src="pics/icons/documentor.png" alt="Documentor" />&#160;';
			}
			print '<br />';
			print '<ul>';
			foreach($author['games'] as $game) {
				$gameIcon = 'games/'.strtolower(trim($game->name)).'/icon.png';
				print '<li>';
				print '<a href=\'game.php?game='.strtolower(trim($game->name)).' \'>'.$game->name.'</a><br />';
				print '</li>';
			}
			print '</ul>';
			
			print '<br/ ><br />';
		}
	}

	include "footer.inc";
?>